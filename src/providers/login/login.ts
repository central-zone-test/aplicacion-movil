import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Platform } from 'ionic-angular';
import { Injectable } from '@angular/core';
import { environment, end_point } from '../../environments/environment';

@Injectable()
export class LoginProvider {

  constructor(public http: HttpClient, public plt: Platform) {
    console.log('Hello LoginProvider Provider');
  }

  async login(email: string, password: string): Promise<any>{
    const headers = new HttpHeaders();
    headers.set('Content-Type', 'application/json; charset=utf-8');
    headers.set('Access-Control-Allow-Origin', '*');
    

    return await this.http.post(this.getBaseUrl() + end_point.LOGIN, 
      {'email': email, 'password': password, 'API-KEY': environment.API_KEY}, {headers: headers}).toPromise();
  }

  getBaseUrl(): string{
    return this.plt.is('cordova') ?  environment.BASE_REAL_URL : environment.BASE_PROXY_URL;
  }

}
