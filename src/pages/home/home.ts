import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {

  private data: any;

  constructor(public navCtrl: NavController, public navParams: NavParams) {

    this.data = this.navParams.get('data');
    if(!this.data){
      this.data = {
        message : 'Hola Bjork',
        data: {
          name : 'Bjork Fig',
          last_name : 'Flask Gdl',
          email : 'arturo.hernandez.condor@gmail.com',
          phone : '5514865461',
          job_title : 'Mobile Developer'
        }
      }
    }

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HomePage');
  }

  

}
