import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';
import { LoginProvider } from '../../providers/login/login';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  private body : FormGroup;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private formBuilder: FormBuilder, public loadingCtrl: LoadingController,
    private loginProvider: LoginProvider, public alertCtrl: AlertController) {

    this.body = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required],
    });

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  async logForm() {
    const loader = this.loadingCtrl.create({
      content: "Iniciando sesión..."
    });
    loader.present();
    try {
      const res = await this.loginProvider.login(this.body.value.email, this.body.value.password);
      if(res.status)
        this.navCtrl.setRoot('HomePage', {data: res});

      loader.dismiss();
    } catch (error) {
      console.log(error);
      console.log(JSON.stringify(error));
      

      const alert = this.alertCtrl.create({
        title: 'Error al Iniciar sesión',
        subTitle: error.error.message,
        buttons: ['OK']
      });
      alert.present();


      loader.dismiss();
    }
  }

}
